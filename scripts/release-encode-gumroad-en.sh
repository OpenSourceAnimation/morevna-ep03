#!/bin/bash

SCRIPTPATH=$(cd `dirname "$0"`; pwd)

SOURCE="${SCRIPTPATH}/../render/ep03-en.blend.avi"
OUTPUT="${SCRIPTPATH}/../render/ep03-en.mp4"
DURATION="00:10:06"

bash ${SCRIPTPATH}/release-encode-gumroad.sh "${SOURCE}" "${OUTPUT}" "${DURATION}"
